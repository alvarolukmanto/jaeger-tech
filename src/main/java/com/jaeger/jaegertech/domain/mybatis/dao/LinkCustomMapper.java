package com.jaeger.jaegertech.domain.mybatis.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.jaeger.jaegertech.domain.mybatis.model.Link;

@Mapper
public interface LinkCustomMapper {

	long selectLinkNextval();

	List<Link> selectLinkWithLimit(@Param("limit") long limit, @Param("offset") long offset);

}