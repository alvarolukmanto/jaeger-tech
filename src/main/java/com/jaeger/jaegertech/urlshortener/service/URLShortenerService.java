package com.jaeger.jaegertech.urlshortener.service;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * <p>
 * URL shortener to shorten & get original URL. Since this is for shortener, the
 * implementation should returns shorter URL than original, in terms of number
 * of character. For example:
 * <table>
 * <th>
 * <td>Original URL</td>
 * <td>Shortened URL</td></th> <tbody>
 * <tr>
 * <td>https://www.mynewssite.com/news/sports/soccess/world-cup-2018/result?country=EN</td>
 * <td>https://my.site/Rk4f</td>
 * </tr>
 * </tbody>
 * </table>
 * </p>
 * 
 * @author timpamungkas
 *
 */
public interface URLShortenerService {

	/**
	 * Get the original URL from shortURL.
	 * 
	 * @param slug shortened URL, full path (include protocol & path)
	 * @return original URL, full path (include protocol & path)
	 * @throws MalformedURLException if <code>returned original URL</code> is not
	 *                               valid URL.
	 */
	URL original(String slug) throws MalformedURLException;

	/**
	 * Get the original URL from shortURL.
	 * 
	 * @param shortURL shortened URL, full path (include protocol & path)
	 * @return original URL, full path (include protocol & path)
	 * @throws MalformedURLException if <code>returned original URL</code> is not
	 *                               valid URL.
	 */
	default URL original(URL shortURL) throws MalformedURLException {
		return original(shortURL.getPath().substring(1));
	}

	/**
	 * Shorten original URL.
	 * 
	 * @param urlString URL string, if not valid URL will throws
	 *                  MalformedURLException
	 * @return shortened URL, slug only
	 * @throws MalformedURLException if <code>urlString</code> is not valid URL.
	 */
	default String shorten(String urlString) throws MalformedURLException {
		return shorten(new URL(urlString));
	}

	/**
	 * Shorten original URL.
	 * 
	 * @param url original URL
	 * @return shortened URL, slug only
	 */
	String shorten(URL url);

}
