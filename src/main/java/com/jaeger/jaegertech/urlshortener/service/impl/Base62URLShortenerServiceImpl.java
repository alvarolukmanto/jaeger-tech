package com.jaeger.jaegertech.urlshortener.service.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jaeger.connpod.util.Base62Converter;
import com.jaeger.jaegertech.domain.mybatis.dao.LinkCustomMapper;
import com.jaeger.jaegertech.domain.mybatis.dao.LinkMapper;
import com.jaeger.jaegertech.domain.mybatis.model.Link;
import com.jaeger.jaegertech.urlshortener.service.URLShortenerService;

@Repository
public class Base62URLShortenerServiceImpl implements URLShortenerService {

	private Base62Converter base62Converter;

	@Autowired
	private LinkMapper linkDAO;

	@Autowired
	private LinkCustomMapper linkCustomDAO;

	public Base62URLShortenerServiceImpl() {
		this.base62Converter = new Base62Converter();
	}

	@Override
	public URL original(String slug) throws MalformedURLException {
		// Get the encoded ID (from slug)
		// Decode to get persistent storage ID
		// Retrieve original URL from persistent

		// get slug
		long linkId = base62Converter.toBase10(slug);
		Link link = linkDAO.selectByPrimaryKey(linkId);

		return link == null ? null : new URL(link.getOriginalUrl());
	}

	@Override
	public String shorten(URL url) {
		// Save to persistent storage, get ID.
		// ID must be consistent to avoid multiple original URL using same ID.
		// ID is the one that encoded/decoded to be returned as shortened URL
		long linkId = linkCustomDAO.selectLinkNextval();

		// hash it using encoder (in this case, Base62Converter)
		String slugBase62 = base62Converter.fromBase10(linkId);

		// insert to DB
		var now = new Date();
		var link = new Link();
		link.setLinkId(linkId);
		link.setSlug(slugBase62);
		link.setOriginalUrl(url.toString());
		link.setCreatedBy(-1);
		link.setCreationDate(now);
		link.setLastUpdatedBy(-1);
		link.setLastUpdatedDate(now);

		linkDAO.insert(link); 

		return slugBase62;
	}

}
