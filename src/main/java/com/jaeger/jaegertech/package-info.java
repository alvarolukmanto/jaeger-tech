/**
 * <code>jaeger-tech</code> is incubator for ungrouped functionalities. Whenever
 * some functionalities became too large, need to revamp to another jaeger
 * project.
 * 
 * @author timpamungkas
 *
 */
package com.jaeger.jaegertech;