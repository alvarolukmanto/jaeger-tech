package com.jaeger.jaegertech.common;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

/**
 * To use jaeger.properties from external file (on production), set environment
 * variable <strong>jaeger.properties</strong> during server start. Refer to
 * <a href=
 * "https://stackoverflow.com/questions/30079255/propertysource-in-a-jar-for-an-external-file-on-the-classpath">this
 * article</a>.<br/>
 * 
 * @author timpamungkas
 *
 */
@PropertySources({ @PropertySource("classpath:jaeger-dev.properties"),
		@PropertySource(value = "${jaeger.properties}", ignoreResourceNotFound = true)

})
@Configuration
public class JaegerTechConfig {
}
