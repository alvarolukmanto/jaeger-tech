package com.jaeger.jaegertech;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JaegerTechApplication implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(JaegerTechApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(JaegerTechApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info(JaegerTechApplication.class.getSimpleName() + " started");
	}

}
