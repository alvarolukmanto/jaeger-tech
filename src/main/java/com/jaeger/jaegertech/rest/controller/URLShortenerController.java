package com.jaeger.jaegertech.rest.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.routines.UrlValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.jaeger.connpod.rest.builder.JsonResponseBuilder;
import com.jaeger.connpod.rest.json.JsonBaseResponse;
import com.jaeger.jaegertech.rest.model.OriginalURL;
import com.jaeger.jaegertech.rest.model.ShortURL;
import com.jaeger.jaegertech.urlshortener.service.URLShortenerService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = { "URL Shortener" }, description = "API for URL shortener & decoder from short URL")
@RestController
public class URLShortenerController {

	private static final Logger log = LoggerFactory.getLogger(URLShortenerController.class);

	private static final int MAX_ORIGINAL_URL_LENGTH = 2000;

	@Autowired
	@Qualifier("base62URLShortenerServiceImpl")
	private URLShortenerService shortener;

	@ApiOperation(hidden = true, value = "Send redirect to original URL")
	@GetMapping(value = "/{slug}")
	public void redirectToOriginal(@PathVariable String slug, HttpServletRequest request,
			HttpServletResponse response) {
		URL url = null;

		try {
			url = shortener.original(slug);

			if (url != null) {
				response.sendRedirect(url.toString());
			} else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND,
						"Original url not found for : " + request.getRequestURI());
			}
		} catch (IOException e) {
			String message = "Invalid URL => " + url + " : " + e.getMessage();
			log.warn(message);
		}
	}

	private static UrlValidator urlValidator = new UrlValidator();

	@ApiOperation(value = "Shorten long URL (original URL). "
			+ "Returned value is only generated slug and does not includes protocol or host.", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@PostMapping(value = "/v1/shortener/shorten", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<JsonBaseResponse<ShortURL>> shorten(
			@RequestBody @ApiParam(value = "Original long URL, full spec, includes: protocol, path, query string. Max length is 2000 chars.", allowEmptyValue = false, allowMultiple = false, example = "https://www.google.com", required = true) OriginalURL originalURL) {
		ShortURL shortURL = null;
		JsonBaseResponse<ShortURL> response = null;
		var builder = new JsonResponseBuilder<ShortURL>();
		long start = System.currentTimeMillis();
		long processTimeMillis = 0;

		if (originalURL.getOriginalURL().length() > MAX_ORIGINAL_URL_LENGTH) {
			String message = "URL too long (max " + MAX_ORIGINAL_URL_LENGTH + ", found "
					+ originalURL.getOriginalURL().length() + ")";
			log.debug(message);

			// no data
			processTimeMillis = System.currentTimeMillis() - start;
			response = builder.addError("URL too long", message, "400").withSuccess(false)
					.withProcessTimeMillis(processTimeMillis).build();

			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			// calling constructor of java.net.URL seems valid for
			// sttps://fffff.com
			if (!urlValidator.isValid(originalURL.getOriginalURL())) {
				throw new MalformedURLException("Invalid URL : " + originalURL.getOriginalURL());
			}

			String slug = shortener.shorten(originalURL.getOriginalURL());

			shortURL = new ShortURL(slug, originalURL.getOriginalURL());
			processTimeMillis = System.currentTimeMillis() - start;

			response = builder.withData(shortURL).withProcessTimeMillis(processTimeMillis).withSuccess(true).build();

			return new ResponseEntity<>(response, HttpStatus.CREATED);
		} catch (MalformedURLException e) {
			String message = "Invalid URL : " + originalURL.getOriginalURL();
			log.warn(message);

			processTimeMillis = System.currentTimeMillis() - start;

			// no data
			response = builder.addError(message, e.getMessage(), "400").withSuccess(false)
					.withProcessTimeMillis(processTimeMillis).build();

			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
	}

}
