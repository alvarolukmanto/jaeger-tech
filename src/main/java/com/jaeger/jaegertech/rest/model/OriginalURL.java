package com.jaeger.jaegertech.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OriginalURL {

	@JsonProperty(value = "original_url")
	private String originalURL;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OriginalURL other = (OriginalURL) obj;
		if (originalURL == null) {
			if (other.originalURL != null)
				return false;
		} else if (!originalURL.equals(other.originalURL))
			return false;
		return true;
	}

	public String getOriginalURL() {
		return originalURL;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((originalURL == null) ? 0 : originalURL.hashCode());
		return result;
	}

	public void setOriginalURL(String originalURL) {
		this.originalURL = originalURL;
	}

	@Override
	public String toString() {
		return "OriginalURL [originalURL=" + originalURL + "]";
	}

}
