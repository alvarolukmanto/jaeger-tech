package com.jaeger.jaegertech.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ShortURL {

	@JsonProperty
	private String slug;

	@JsonProperty(value = "original_url")
	private String originalURL;

	public ShortURL(String slug, String originalURL) {
		super();
		this.slug = slug;
		this.originalURL = originalURL;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShortURL other = (ShortURL) obj;
		if (slug == null) {
			if (other.slug != null)
				return false;
		} else if (!slug.equals(other.slug))
			return false;
		return true;
	}

	public String getOriginalURL() {
		return originalURL;
	}

	public String getSlug() {
		return slug;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((slug == null) ? 0 : slug.hashCode());
		return result;
	}

	public void setOriginalURL(String originalURL) {
		this.originalURL = originalURL;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	@Override
	public String toString() {
		return "ShortURL [slug=" + slug + ", originalURL=" + originalURL + "]";
	}

}
