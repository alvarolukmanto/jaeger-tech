package com.jaeger.jaegertech;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectPackages(value = { "com.jaeger.jaegertech.rest", "com.jaeger.jaegertech.urlshortener" })
public class AllTests {

}
