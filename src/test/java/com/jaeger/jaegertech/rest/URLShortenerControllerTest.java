package com.jaeger.jaegertech.rest;

import static java.time.Duration.ofMillis;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jaeger.jaegertech.rest.model.OriginalURL;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class URLShortenerControllerTest {

	private static final String ENDPOINT_SHORTEN = "/v1/shortener/shorten";

	static ObjectMapper objectMapper;

	@BeforeAll
	static final void setup() {
		objectMapper = new ObjectMapper();
	}

	@Autowired
	private MockMvc mockMvc;

	@Disabled
	@Test
	final void testRedirectToOriginal() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	final void testShorten_InvalidURL() throws Exception {
		var invalidUrls = List.of("hdttp://www.det@ik.com?babi<>panggang=nguik", "sttps://dzone.com/",
				"httpx://dzone.com/articles/unit-and-integration-tests-in-spring-boot-2",
				"ttp://dzone.com/articles/why-cios-hold-the-keys-to-growth",
				"htt://stackoverflow.com/questions/23559814/package-hierarchy-of-large-scale-projects/23560492",
				"a common string", "http://non encoded url with space", "http://non,encoded,url?isit=true",
				"http://nonurlcom?@test", "https:////nonsafechars.com?<<>");
		RequestBuilder requestBuilder = null;
		OriginalURL originalURL = new OriginalURL();
		String jsonOriginalUrl = null;
		for (var url : invalidUrls) {
			originalURL.setOriginalURL(url);
			jsonOriginalUrl = objectMapper.writeValueAsString(originalURL);

			requestBuilder = MockMvcRequestBuilders.post(ENDPOINT_SHORTEN)
					.header(HttpHeaders.AUTHORIZATION, DefaultRestControllerTest.basicAuthString)
					.contentType(MediaType.APPLICATION_JSON).content(jsonOriginalUrl);

			mockMvc.perform(requestBuilder).andExpect(status().isBadRequest())
					.andExpect(content().string(containsString("header")))
					.andExpect(content().string(containsString("errors")))
					.andExpect(content().string(containsString("process_time")));
		}
	}

	@Test
	final void testShorten_LengthyURL() throws Exception {
		var sb = new StringBuilder("http://www.xyz.com");
		String lengthyUrl;

		for (int i = 0; i < 200; i++) {
			sb.append("/");
			sb.append(RandomStringUtils.randomAlphabetic(10));
		}

		lengthyUrl = sb.toString();

		var invalidUrls = List.of(lengthyUrl, lengthyUrl.substring(0, 2002));

		RequestBuilder requestBuilder = null;
		OriginalURL originalURL = new OriginalURL();
		String jsonOriginalUrl = null;

		for (var url : invalidUrls) {
			originalURL.setOriginalURL(url);
			jsonOriginalUrl = objectMapper.writeValueAsString(originalURL);

			requestBuilder = MockMvcRequestBuilders.post(ENDPOINT_SHORTEN)
					.header(HttpHeaders.AUTHORIZATION, DefaultRestControllerTest.basicAuthString)
					.contentType(MediaType.APPLICATION_JSON).content(jsonOriginalUrl);

			mockMvc.perform(requestBuilder).andExpect(status().isBadRequest())
					.andExpect(content().string(containsString("header")))
					.andExpect(content().string(containsString("errors")))
					.andExpect(content().string(containsString("process_time")));
		}
	}

	@Test
	final void testShorten_ValidURL() throws Exception {
		var validUrls = List.of("http://www.detik.com", "https://dzone.com/",
				"https://dzone.com/articles/unit-and-integration-tests-in-spring-boot-2",
				"https://dzone.com/articles/why-cios-hold-the-keys-to-growth",
				"https://stackoverflow.com/questions/23559814/package-hierarchy-of-large-scale-projects/23560492",
				"http://COMPANY.COM/CICSPLEXSM/JSMITH/MENU/OURHOME?CONTEXT=FRED&SCOPE=FRED",
				"http://MVSXX.COMPANY.COM/CICSPLEXSM/JSMITH/VIEW/OURTASK?A_PRIORITY=200&O_PRIORITY=GT",
				"http://MVSXX.COMPANY.COM/CICSPLEXSM/JSMITH/VIEW/OURLOCTRAN?A_TRANID=P*&O_TRANID=NE",
				"http://MVSXX.COMPANY.COM/CICSPLEXSM/TOXTETH/VIEW/EYUSTARTPROGRAM.TABULAR?FILTERC=1",
				"http://MVSXX.COMPANY.COM/CICSPLEXSM/JSMITH/VIEW/OURWLMAWAOR.TABLE1?P_WORKLOAD=WLDPAY01");
		OriginalURL originalURL = new OriginalURL();
		String jsonOriginalUrl = null;

		for (var url : validUrls) {
			originalURL.setOriginalURL(url);
			jsonOriginalUrl = objectMapper.writeValueAsString(originalURL);

			final var requestBuilder = MockMvcRequestBuilders.post(ENDPOINT_SHORTEN)
					.header(HttpHeaders.AUTHORIZATION, DefaultRestControllerTest.basicAuthString)
					.contentType(MediaType.APPLICATION_JSON).content(jsonOriginalUrl);

			assertTimeout(ofMillis(200), () -> {
				mockMvc.perform(requestBuilder).andExpect(status().is2xxSuccessful())
						.andExpect(content().string(containsString("header")))
						.andExpect(content().string(containsString("data")))
						.andExpect(content().string(containsString("process_time")));
			});
		}
	}

}
