package com.jaeger.jaegertech.urlshortener;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.jaeger.jaegertech.domain.mybatis.dao.LinkCustomMapper;
import com.jaeger.jaegertech.urlshortener.service.URLShortenerService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class Base62URLShortenerServiceTest {

	private static final long PERFORMANCE_MILLIS_AVERAGE_TRESHOLD = 20;

	private static final long PERFORMANCE_MILLIS_MAX_TRESHOLD = 2 * PERFORMANCE_MILLIS_AVERAGE_TRESHOLD;

	private static List<URL> urls;

	private static final Logger log = LoggerFactory.getLogger(Base62URLShortenerServiceTest.class);

	@BeforeAll
	private static final void initialize() throws MalformedURLException {
		urls = List.of(new URL("http://www.detik.com/"), new URL("https://www.kompas.com/"),
				new URL("https://github.com/airbnb"));
	}

	@Autowired
	@Qualifier("base62URLShortenerServiceImpl")
	private URLShortenerService shortener;

	@Autowired
	private LinkCustomMapper linkCustomDAO;

	// run this first to initialize datasource connection (will take quite ms)
	@BeforeEach
	final void dummyOpenConnection() {
		String shortUrl = null;
		try {
			shortUrl = shortener.shorten("https://www.google.com");
			shortUrl = shortener.shorten("https://www.facebook.com");
		} catch (MalformedURLException e) {
			fail(e.getMessage());
		}
		assertNotNull(shortUrl);
	}

	// there's at least 2 URL
	@Test
	final void testOriginal() {
		for (int i = 10; i <= 12; i++) {
			URL original = null;
			try {
				original = shortener.original(Integer.toString(i));
				assertNotNull(original);
			} catch (MalformedURLException e) {
				fail("MalformedURLException " + original + " : " + e.getMessage());
			}
		}

		for (int i = 0; i <= 1; i++) {
			URL original = null;
			try {
				original = shortener.original(Integer.toString(i));
				assertNull(original);
			} catch (MalformedURLException e) {
				fail("MalformedURLException " + original + " : " + e.getMessage());
			}
		}
	}

	@Test
	final void testOriginalFromURL() {
		int loopCount = 5;
		var links = linkCustomDAO.selectLinkWithLimit(loopCount, 0);

		// translate slug
		for (int i = 0; i < links.size(); i++) {
			var slug = links.get(i).getSlug();
			URL original = null;
			try {
				var dummy = new URL("http://jaeger-tech.com/" + slug);
				original = shortener.original(dummy);

				assertEquals(links.get(i).getOriginalUrl(), original.toString());
			} catch (MalformedURLException e) {
				fail("MalformedURLException " + original + " : " + e.getMessage());
			}

		}
	}

	@Test
	final void testOriginalPerformanceAverage() {
		int loopCount = 10000;
		long start, processTime = 0, min = 1000000l, max = 0;
		double total = 0, average = 0;
		final NumberFormat nf = new DecimalFormat("#.00");
		var links = linkCustomDAO.selectLinkWithLimit(loopCount, 0);

		for (var i = 0; i < links.size(); i++) {
			var slug = links.get(i).getSlug();

			try {
				start = System.currentTimeMillis();
				@SuppressWarnings("unused")
				var original = shortener.original(slug);
				processTime = System.currentTimeMillis() - start;

				total += processTime;

				if (processTime < min) {
					min = processTime;
				}

				if (processTime > max) {
					max = processTime;
				}

				if (i % 1e3 == 0) {
					double percentage = (i * 100) / loopCount;

					log.debug("testOriginalPerformanceAverage : " + nf.format(percentage) + "%");
				}
			} catch (MalformedURLException e) {
				// ignore, this means shortener works fine, but the data is invalid
				log.warn("Invalid URL entry : " + links.get(i).getOriginalUrl());
			}
		}

		log.debug("testOriginalPerformanceAverage : 100%");

		average = total / loopCount;

		String message = "Expected average : " + PERFORMANCE_MILLIS_AVERAGE_TRESHOLD + "ms, actual : " + average + "ms";
		assertTrue(message, average <= PERFORMANCE_MILLIS_AVERAGE_TRESHOLD);
		log.info(message);

		message = "Expected max : " + PERFORMANCE_MILLIS_MAX_TRESHOLD + "ms, actual : " + max + "ms";
		assertTrue(message, max <= PERFORMANCE_MILLIS_MAX_TRESHOLD);
		log.info(message);
	}

	@Test
	final void testShorten() {
		for (URL url : urls) {
			var shortened = shortener.shorten(url);
			assertNotNull(url.toString(), shortened);
		}
	}

	@Test
	final void testShortenPerformanceAverage() {
		var loopCount = 10000;
		long start, processTime = 0, min = 1000000l, max = 0;
		double total = 0, average = 0;
		final NumberFormat nf = new DecimalFormat("#.00");

		for (var i = 0; i < loopCount; i++) {
			URL url = urls.get(ThreadLocalRandom.current().nextInt(urls.size()));

			start = System.currentTimeMillis();
			shortener.shorten(url);
			processTime = System.currentTimeMillis() - start;

			total += processTime;

			if (processTime < min) {
				min = processTime;
			}

			if (processTime > max) {
				max = processTime;
			}

			if (i % 1e3 == 0) {
				double percentage = (i * 100) / loopCount;

				log.debug("testShortenPerformanceAverage : " + nf.format(percentage) + "%");
			}
		}

		log.debug("testShortenPerformanceAverage : 100%");

		average = total / loopCount;

		String message = "Expected average : " + PERFORMANCE_MILLIS_AVERAGE_TRESHOLD + "ms, actual : " + average + "ms";
		assertTrue(message, average <= PERFORMANCE_MILLIS_AVERAGE_TRESHOLD);
		log.info(message);

		message = "Expected max : " + PERFORMANCE_MILLIS_MAX_TRESHOLD + "ms, actual : " + max + "ms";
		assertTrue(message, max <= PERFORMANCE_MILLIS_MAX_TRESHOLD);
		log.info(message);
	}

}
